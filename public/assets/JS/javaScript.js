
  canvas = document.getElementById("the_canvas");
  context = canvas.getContext("2d");  

  let charSelector = 0;

function currentTime() {

    let date = new Date(); 
    let hh = date.getHours();
    let mm = date.getMinutes();
    let ss = date.getSeconds();
    let session = "AM";
  
    if(hh === 0){
        hh = 12;
    }
    if(hh > 12){
        hh = hh - 12;
        session = "PM";
     }
  
     hh = (hh < 10) ? "0" + hh : hh;
     mm = (mm < 10) ? "0" + mm : mm;
     ss = (ss < 10) ? "0" + ss : ss;
      
     let time = hh + ":" + mm + ":" + ss + " " + session;
  
    document.getElementById("clock").innerText = time; 
    let t = setTimeout(function(){ currentTime() }, 1000);


    let d = new Date();
      let day =d.getDate();
      let month = d.getMonth()+1;
      let year = d.getFullYear();
  
  
      let dPrint = day + "/" + month + "/" + year;
      document.getElementById("date").innerText = dPrint;   

    }


    let image1 = new Image();
    image1.src = "assets/img/amongus.png";
     let image2 = new Image();
    image2.src = "assets/img/sus.png";
    
    // GameObject holds positional information
    // Can be used to hold other information based on requirements
    function GameObject(spritesheet, x, y, width, height) {
        this.spritesheet = spritesheet;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    // Default Player
    let player = new GameObject(image1, 20, 230, 200, 200);
    let player2 = new GameObject(image2, 0, 0, 200, 200);  


    function GamerInput(input) {
        this.action = input; // Hold the current input as a string
    }
    
    // Default GamerInput is set to None
    let gamerInput = new GamerInput("None"); //No Input
    
    
    function input(event) {
 
        if (event.type === "keydown") {
            switch (event.keyCode) {
                case 65: // Left Arrow
                    gamerInput = new GamerInput("Left");
                    break; //Left key
                case 87: // Up Arrow
                    gamerInput = new GamerInput("Up");
                    break; //Up key
                case 68: // Right Arrow
                    gamerInput = new GamerInput("Right");
                    break; //Right key
                case 83: // Down Arrow
                    gamerInput = new GamerInput("Down");
                    break; //Down key
                case 32:
                    gamerInput = new GamerInput ("Space");
                    charSelector++;
                    break;    
                case 13:
                    gamerInput = new GamerInput ("Enter");
                break;    
                        
                default:
                    gamerInput = new GamerInput("None"); //No Input
            }
        } else {
            gamerInput = new GamerInput("None");
        }
    }
    
    let speed = 4;
    
    function update() {
        // console.log("Update");
        // Check Input
        if (gamerInput.action === "Up")
        {
         
            player.y -= speed; // Move Player Up
        }
         else if (gamerInput.action === "Down")
        {
            player.y += speed; // Move Player Down
        }
        else if (gamerInput.action === "Left")
        {
          
            player.x -= speed; // Move Player Left
        }
        else if (gamerInput.action === "Right")
        {
       
            player.x += speed; // Move Player Right
        }
        else if(gamerInput.action == "Space")
        {
            if (charSelector % 2 === 0){
                player.spritesheet= image1;
            }
            else{
                player.spritesheet= image2;
            }

        }
        else if(gamerInput.action == "Enter")
        {
            music();
        }
    }


    function changeCharacter()
    {
        if(spacePressed == "true")
        {
            player = new GameObject(image2, 20, 230, 200, 200);
           
        }
    }

    function music()
    {
    
    let sound= new Audio;
    sound.src= "assets/media/audio.mp3";
    sound.play();
    }


    function draw() {
        // Clear Canvas
        context.clearRect(0, 0, canvas.width, canvas.height);
        //console.log("Draw");
        // console.log(player);
        context.drawImage(player.spritesheet, 
                          player.x,
                          player.y,
                          player.width,
                          player.height);
    }
    
    function gameloop() {
        update();
        draw();
        window.requestAnimationFrame(gameloop);
    }
    



    window.requestAnimationFrame(gameloop);
    window.addEventListener('keydown', input);
    window.addEventListener('keyup', input);
    







currentTime();
